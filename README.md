<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.90 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.12 -->
# db_pg 0.3.7

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_db_pg/develop?logo=python)](
    https://gitlab.com/ae-group/ae_db_pg)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_db_pg/release0.3.6?logo=python)](
    https://gitlab.com/ae-group/ae_db_pg/-/tree/release0.3.6)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_db_pg)](
    https://pypi.org/project/ae-db-pg/#history)

>ae namespace module portion db_pg: postgres database layer.

[![Coverage](https://ae-group.gitlab.io/ae_db_pg/coverage.svg)](
    https://ae-group.gitlab.io/ae_db_pg/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_db_pg/mypy.svg)](
    https://ae-group.gitlab.io/ae_db_pg/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_db_pg/pylint.svg)](
    https://ae-group.gitlab.io/ae_db_pg/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_db_pg)](
    https://gitlab.com/ae-group/ae_db_pg/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_db_pg)](
    https://gitlab.com/ae-group/ae_db_pg/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_db_pg)](
    https://gitlab.com/ae-group/ae_db_pg/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_db_pg)](
    https://pypi.org/project/ae-db-pg/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_db_pg)](
    https://gitlab.com/ae-group/ae_db_pg/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_db_pg)](
    https://libraries.io/pypi/ae-db-pg)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_db_pg)](
    https://pypi.org/project/ae-db-pg/#files)


## installation


execute the following command to install the
ae.db_pg module
in the currently active virtual environment:
 
```shell script
pip install ae-db-pg
```

if you want to contribute to this portion then first fork
[the ae_db_pg repository at GitLab](
https://gitlab.com/ae-group/ae_db_pg "ae.db_pg code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_db_pg):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_db_pg/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.db_pg.html
"ae_db_pg documentation").
